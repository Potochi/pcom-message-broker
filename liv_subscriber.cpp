#include "liv_subscriber.hpp"
#include <netinet/tcp.h>
#include <unistd.h>

liv_subscriber::liv_subscriber(const string& id, const string& ip,
                               const string& port)
    : id(id), running(1) {

    cin.exceptions(istream::failbit | istream::badbit);
    ::memset(&readfds, 0, sizeof(readfds));

    in_addr tmp;
    CCALL(::inet_aton(ip.c_str(), &tmp), "Invalid IP address!");

    int cport = static_cast<int>(stoi(port));

    if (cport > numeric_limits<u16>::max()) {
        throw invalid_argument("Invalid port");
    }

    sockaddr_in sockad;
    ::memset(&sockad, 0, sizeof(sockad));

    sockad.sin_addr = tmp;
    sockad.sin_family = AF_INET;
    sockad.sin_port = htons(static_cast<u16>(cport));

    int con_sock = CCALL(::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP),
                         "Failed to open socket");

    const int yes = 1;
    CCALL(::setsockopt(con_sock, SOL_TCP, TCP_NODELAY, &yes, sizeof(yes)),
          "Failed to set TCP_NODELAY");

    CCALL(::connect(con_sock, (sockaddr*)&sockad, sizeof(sockad)),
          "Failed to connect to server");

    tsock._fd = con_sock;

    id_payload peyload{id};
    liv_tcp_packet ident{move(peyload)};

    ident.serialize_to(tsock);

    FD_SET(tsock._fd, &readfds);
    FD_SET(STDIN_FILENO, &readfds);

    maxfd = max(tsock._fd, STDIN_FILENO);
}

void liv_subscriber::run() {
    try {
        while (running) {
            auto t = await_input();

            switch (t) {
            case MESSAGE_INPUT: {
                handle_message();
            } break;
            case USER_INPUT: {
                handle_user_input();
            } break;
            case ERROR: {
                running = 0;
            } break;
            }
        }
    } catch (int x) {
        if (x == 0) {
            cerr << "Connection to server was closed";
        } else {
            cerr << "Error " << strerror(x) << " occured";
        }
    }

    tsock.close();
}

input_type liv_subscriber::await_input() {
    fd_set tmpset = readfds;

    CCALL(::select(maxfd + 1, &tmpset, NULL, NULL, NULL));

    if (FD_ISSET(tsock._fd, &tmpset)) {
        return MESSAGE_INPUT;
    }
    if (FD_ISSET(STDIN_FILENO, &tmpset)) {
        return USER_INPUT;
    }

    return ERROR;
}

void liv_subscriber::handle_message() {
    liv_tcp_packet packet;

    packet.deserialize_from(tsock);

    const message_payload& mes = get<message_payload>(packet.get_payload());

    in_addr temp;
    temp.s_addr = mes.packet.get_addr();
    char* sender_ip = inet_ntoa(temp);
    u16 sender_port = ntohs(mes.packet.get_port());
    const string& topic = mes.packet.get_topic();

    cout << sender_ip << ":" << sender_port << " - " << topic << " - ";

    switch (mes.packet.get_data_type()) {
    case 0: {
        cout << "INT - ";
        u8 sign = mes.packet.get_data().data()[0];
        u32 value = ntohl(*(u32*)(&mes.packet.get_data().data()[1]));
        if (sign) {
            cout << "-";
        }
        cout << value << endl;

    } break;
    case 1: {
        cout << "SHORT_REAL - ";
        u16 value = ntohs(*(u16*)(mes.packet.get_data().data()));
        printf("%d.%02d\n", value / 100, value % 100);

    } break;
    case 2: {
        cout << "FLOAT - ";
        u8 sign = mes.packet.get_data().data()[0];
        u32 value = ntohl(*(u32*)(&mes.packet.get_data().data()[1]));
        u8 exponent = *(u32*)(&mes.packet.get_data().data()[5]);
        u32 power = 1;
        for (int i = 0; i < exponent; i++) {
            power *= 10;
        }
        if (sign) {
            printf("-");
        }
        printf("%d.%0*d\n", value / power, exponent, value % power);

    } break;
    case 3: {
        cout << "STRING - ";
        printf("%s\n", mes.packet.get_data().data());
    } break;
    }
}

void liv_subscriber::handle_user_input() {
    management_payload peyload;
    user_action action = INVALID;
    try {
        cin >> peyload.verb;

        if (peyload.verb == "subscribe") {
            action = SUBSCRIBE;
            cin >> peyload.topic >> peyload.sf;
            if (!(peyload.sf == 0 || peyload.sf == 1)) {
                throw invalid_argument("Invalid sf");
            }

        } else if (peyload.verb == "unsubscribe") {
            cin >> peyload.topic;
            action = UNSUBSCRIBE;
        } else if (peyload.verb == "exit") {
            running = 0;
            return;
        }
    } catch (const exception& e) {
        cerr << "Invalid input! " << e.what() << endl;
        cin.clear();
        cin.ignore(numeric_limits<int>::max(), '\n');
        return;
    }

    liv_tcp_packet packet{move(peyload)};
    packet.serialize_to(tsock);

    switch (action) {
    case SUBSCRIBE: {
        cout << "Subscribed to topic.\n";
    } break;
    case UNSUBSCRIBE: {
        cout << "Unsubscribed from topic.\n";
    } break;
    case INVALID: {
        cerr << "Command " << peyload.verb << " is invalid" << endl;
        cin.ignore(numeric_limits<int>::max(), '\n');
    } break;
    }
}
