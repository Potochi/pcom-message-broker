#pragma once

#include "common.hpp"
#include "liv_socket.hpp"
#include <string>
#include <type_traits>
#include <vector>

using namespace std;

namespace liv {

template <typename T>
requires is_trivial_v<T> void serialize(tcp_socket& sock, T v) {
    sock.send(&v, sizeof(v));
}

template <typename T>
requires is_trivial_v<T> void deserialize(tcp_socket& sock, T& v) {
    sock.recv(&v, sizeof(v));
}

template <typename T>
requires(!is_trivial_v<T>) void serialize(tcp_socket& sock, T& v) {
    serialize(sock, v.size());
    sock.send(v.data(), sizeof(decltype(*begin(v))) * v.size());
}

template <typename T>
requires(!is_trivial_v<T>) void deserialize(tcp_socket& sock, T& v) {
    size_t vec_size;
    deserialize(sock, vec_size);
    v.resize(vec_size);
    sock.recv(v.data(), sizeof(decltype(*begin(v))) * vec_size);
}

}; // namespace liv
