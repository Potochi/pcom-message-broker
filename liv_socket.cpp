#include "liv_socket.hpp"

#include "liv_utils.hpp"
#include <arpa/inet.h>
#include <errno.h>
#include <sys/socket.h>
#include <type_traits>
#include <unistd.h>
#include <vector>

namespace liv {
tcp_socket::tcp_socket(int fd) {
    gte(fd, 0, "Trying to create liv_socket with an invalid socket");
    this->_fd = fd;
}

void tcp_socket::close() {
    if (_fd >= 0) {
        CCALL(::shutdown(_fd, SHUT_RDWR), "Socket shutdown failed");
        CCALL(::close(_fd), "Socket close failed");
        _fd = -1;
    }
}

void tcp_socket::send(const void* data, ssize_t size) {

    ssize_t sent = 0;
    u8* write_head = (u8*)data;
    while (sent != size) {

        ssize_t ret = CCALL(::send(_fd, write_head, size - sent, 0),
                            "Failed to send to socket");
        if (ret == 0) {
            throw 0;
        }

        sent += ret;
        write_head += ret;
    }
}

void tcp_socket::recv(void* data, ssize_t size) {
    ssize_t received = 0;
    u8* write_head = (u8*)data;

    while (received != size) {
        ssize_t ret = CCALL(::recv(_fd, write_head, size - received, 0),
                            "Failed to receive from socket");
        if (ret == 0) {
            throw 0;
        }

        received += ret;
        write_head += ret;
    }
}

}; // namespace liv
