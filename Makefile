CC := g++

COMMON_SOURCES := liv_socket.cpp liv_udp_packet.cpp liv_tcp_packet.cpp liv_utils.cpp

SERVER_SOURCES := server.cpp $(COMMON_SOURCES) liv_udp_sink.cpp liv_client_manager.cpp liv_client.cpp

SUBSCRIBER_SOURCES := subscriber.cpp $(COMMON_SOURCES) liv_subscriber.cpp

all: build

build: server subscriber

server: $(SERVER_SOURCES)
	$(CC) -std=c++20 $(SERVER_SOURCES) -o server

subscriber: $(SUBSCRIBER_SOURCES)
	$(CC) -std=c++20 $(SUBSCRIBER_SOURCES) -o subscriber

clean:
	rm -f server subscriber
