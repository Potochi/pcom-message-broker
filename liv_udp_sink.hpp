#pragma once

#include "common.hpp"
#include "liv_udp_packet.hpp"
#include <string>

using namespace std;

class liv_udp_sink {
public:
    liv_udp_sink(u16 port);
    liv_udp_packet receive_packet();

public:
    int _fd;
};
