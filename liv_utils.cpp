#include "liv_utils.hpp"

namespace liv {
void _internal_assert_fail(const string& msg) {
    cerr << "Asserion failure: " << msg << endl;
    exit(-1);
}
} // namespace liv
