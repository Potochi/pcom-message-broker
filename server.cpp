#include "liv_client_manager.hpp"

#include <iostream>

using namespace std;

void usage() {
    cerr << "./server <port>\n";
}

int main(int argc, char** argv) {

    if (argc != 2) {
        usage();
        return -1;
    }


    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);

    try {
        liv_client_manager manager{argv[1]};
        manager.run();

    } catch (const invalid_argument& e) {
        cerr << "Invalid port" << endl;
    } catch (const out_of_range& e) {
        cerr << "Invalid port" << endl;
    }
}
