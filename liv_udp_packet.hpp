#pragma once

#include "common.hpp"
#include "liv_socket.hpp"
#include <arpa/inet.h>
#include <vector>

using namespace std;
using namespace liv;
class liv_udp_packet {

public:
    liv_udp_packet();
    liv_udp_packet(const char* topic, u8 data_type, u8* raw_data,
                   size_t data_size);
    liv_udp_packet(const char* topic, u8 data_type, u8* raw_data,
                   size_t data_size, const sockaddr_in& sender);

    const string& get_topic() const;
    const vector<u8>& get_data() const;
    u8 get_data_type() const;
    u16 get_port() const;
    u32 get_addr() const;

    void serialize_to(tcp_socket& sock);
    void deserialize_from(tcp_socket& sock);

    void debug();

private:
    string topic;
    vector<u8> data;
    u8 data_type;
    u16 sender_port;
    u32 sender_addr;
};
