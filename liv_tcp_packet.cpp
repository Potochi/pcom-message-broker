#include "liv_tcp_packet.hpp"
#include "liv_serialize.hpp"

liv_tcp_packet::liv_tcp_packet(management_payload&& management) {
    type = MANAGEMENT;
    payload.emplace<management_payload>(management);
}

liv_tcp_packet::liv_tcp_packet(message_payload&& message) {
    type = MESSAGE;
    payload.emplace<message_payload>(message);
}

liv_tcp_packet::liv_tcp_packet(liv_udp_packet&& packet) {
    type = MESSAGE;
    payload.emplace<message_payload>(move(packet));
}

liv_tcp_packet::liv_tcp_packet(id_payload&& id) {
    type = ID;
    payload.emplace<id_payload>(id);
}

void liv_tcp_packet::serialize_to(tcp_socket& sock) {
    serialize(sock, type);
    switch (type) {
    case MANAGEMENT: {
        management_payload& man = get<management_payload>(payload);
        serialize(sock, man.sf);
        serialize(sock, man.topic);
        serialize(sock, man.verb);

    } break;
    case ID: {
        id_payload& id = get<id_payload>(payload);
        serialize(sock, id.user_id);

    } break;
    case MESSAGE: {
        message_payload& msg = get<message_payload>(payload);
        msg.packet.serialize_to(sock);

    } break;
    }
}

void liv_tcp_packet::deserialize_from(tcp_socket& sock) {
    deserialize(sock, type);
    switch (type) {
    case MANAGEMENT: {
        management_payload man;

        deserialize(sock, man.sf);
        deserialize(sock, man.topic);
        deserialize(sock, man.verb);

        payload.emplace<management_payload>(move(man));

    } break;
    case ID: {
        id_payload id;
        deserialize(sock, id.user_id);

        payload.emplace<id_payload>(move(id));

    } break;
    case MESSAGE: {
        message_payload msg;
        msg.packet.deserialize_from(sock);

        payload.emplace<message_payload>(move(msg));

    } break;
    }
}

packet_type liv_tcp_packet::get_type() const {
    return type;
}

const payload_variant& liv_tcp_packet::get_payload() const {
    return payload;
}
