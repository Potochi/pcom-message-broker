#include "liv_udp_sink.hpp"

#include <arpa/inet.h>
#include <sys/socket.h>

using namespace liv;

liv_udp_sink::liv_udp_sink(u16 port) {
    _fd = CCALL(::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP),
                "Failed to open udp sink socket");

    sockaddr_in udp_sink;
    ::memset(&udp_sink, 0, sizeof(udp_sink));

    udp_sink.sin_addr.s_addr = INADDR_ANY;
    udp_sink.sin_family = AF_INET;
    udp_sink.sin_port = htons(port);

    const int yes = 1;
    CCALL(::setsockopt(_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)),
          "Failed to setsockopt SO_REUSEADDR on udp sink");

    CCALL(::bind(_fd, (sockaddr*)&udp_sink, sizeof(udp_sink)),
          "Failed to bind udp sink socket");
}

liv_udp_packet liv_udp_sink::receive_packet() {
    u8 tmp_buf[2048];

    sockaddr_in sender;
    socklen_t sender_len = sizeof(sender);

    ::memset(&sender, 0, sizeof(sender));

    ssize_t received = CCALL(::recvfrom(_fd, tmp_buf, sizeof(tmp_buf), 0,
                                        (sockaddr*)&sender, &sender_len),
                             "Failed to read from udp sink socket");
    char tmp_topic[51];
    ::sprintf(tmp_topic, "%.50s", (char*)tmp_buf);
    gte(received, 51L, "Did not receive enough udp data");
    return liv_udp_packet(tmp_topic, tmp_buf[50], &tmp_buf[51], received - 51,
                          sender);
}
