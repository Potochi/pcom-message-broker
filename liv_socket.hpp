#pragma once

#include "liv_types.hpp"

using namespace std;

namespace liv {
class tcp_socket {
public:
    tcp_socket() = default;
    tcp_socket(int fd);
    void send(const void* data, ssize_t size);
    void recv(void* data, ssize_t size);

    int _fd;
    void close();
};
}; // namespace liv
