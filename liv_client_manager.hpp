#pragma once

#include "common.hpp"
#include "liv_client.hpp"
#include "liv_tcp_packet.hpp"
#include "liv_udp_sink.hpp"
#include <unordered_map>
#include <vector>

using namespace std;
using namespace liv;

struct client_entry {
    shared_ptr<liv_client> client_ptr;
    bool sf;
};

class liv_client_manager {
public:
    liv_client_manager(const string& port);

    void accept_connection();
    void read_packets(fd_set read_ready);
    void read_udp();

    void run();

private:
    unordered_map<string, unordered_map<string, client_entry>> buckets;
    unordered_map<string, shared_ptr<liv_client>> clients;
    void distribute(shared_ptr<liv_tcp_packet> packet);
    void subscribe(shared_ptr<liv_client> client, string topic, bool sf);
    void unsubscribe(shared_ptr<liv_client> client, string topic);
    void set_socket(int fd);
    void unset_socket(int fd);
    optional<liv_udp_sink> _udp_sink;

    int _listen_fd;
    int _udp_fd;
    fd_set _user_fds;
    int _max_fd;
    int running;
};
