                  ____________________________________

                   PROTOCOALE DE COMUNICATIE - TEMA 2

                       Bud Liviu-Alexandru 321CA
                  ____________________________________


Table of Contents
_________________

1. Instructiuni compilare
.. 1. Important
2. Instructiuni testare
3. Program flow
.. 1. Server
.. 2. Subscriber
4. Detalii implementare
.. 1. General
..... 1. Trimiterea datelor pe retea (1)
..... 2. Trimiterea datelor pe retea (2)
..... 3. Structura pachetelor UDP
..... 4. Structura pachetelor TCP
..... 5. Memory management
.. 2. Server


Pentru testarea temei am ales sa folosesc clientul UDP oferit in
schelet, acesta se afla in folderul `pcom_hw2_udp_client'.


1 Instructiuni compilare
========================

  Pentru a compila executabilele `server' si `subscriber' se ruleaza
  comanda `make'. Pentru stergerea executabilelor se ruleaza `make
  clean'.

  Tema a fost compilata si testata local cu compilatorul `g++',
  versiunea 10.2.0.


1.1 Important
~~~~~~~~~~~~~

  Pentru compilarea temei este necesara o versiune de g++ cu suport
  pentru standardul C++20 (datorita utilizarii conceptelor in declararea
  unor template-uri)


2 Instructiuni testare
======================

  Pentru a testa functionalitatea temei in mod automat se poate rula
  comanda `sudo python test.py'. Pentru o testare manuala se poate porni
  serverul cu comanda `./server <port>' si subscriberul cu comanda
  `./subscriber <id> <ip_server> <port_server>'. Pentru a trimite mesaje
  se poate folosi clientul UDP oferit in schelet.


3 Program flow
==============

3.1 Server
~~~~~~~~~~

  - Se valideaza argumentele date la lina de comanda
  - Se creeaza socketul udp si listener-ul tcp
  - Se asteapta conexiuni/mesaje de la clienti conectati/mesaje UDP cu
    ajutorul multiplexarii IO folosind `select' si input de la tastatura
    + In cazul in care s-a primit un pachet UDP, acesta este trimis la
      toti clientii conectati si TCP abonati la acel topic
      + Daca clientul nu este conectat si are flagul `SF'
        (store-and-forward) setat pentru topic mesajul se salveaza
        intr-un queue pentru a fi trimis mai tarziu
    + In cazul in care s-a primit un mesaj de subscribe/unsubscribe se
      adauga/scoate clientul respectiv din lista abonatilor la topicul
      cerut
    + In cazul in care exista o conexiune noua se accepta conexiunea si
      se asteapta primirea ID-ului clientului pentru a fi adaugat in
      lista de clienti
    + In cazul in care exista input la tastatura acesta este citit, si
      daca a fost introdusa comanda `exit' serverul inchide toate
      conexiunile si se inchide


3.2 Subscriber
~~~~~~~~~~~~~~

  - Se valideaza argumentele date la linia de comanda
  - Se deschide o conexiune TCP catre ip-ul si port-ul dat la linia de
    comanda
  - Se asteapta pachete/input de la linia de comanda
    + In cazul in care s-a primit input de la linia de comanda se
      valideaza comanda data, fiind posibile doar 3 comenzi
      (`subscribe', `unsubscribe' si `exit'). Se valideaza parametrii
      lor si se trimit catre server.
    + In cazul incare s-a primit un mesaj, continutul este parsat si
      afisat la ecran.


4 Detalii implementare
======================

4.1 General
~~~~~~~~~~~

4.1.1 Trimiterea datelor pe retea (1)
-------------------------------------

  Deoarece api-ul din `sockets.h' nu ne garanteaza ca pe retea se
  trimite/primeste numarul de octeti dati ca parametru functiei, am ales
  sa implementez un wrapper pe langa socket, numita `tcp_socket'.
  Aceasta clasa se afla in fisierul <./liv_socket.hpp>, si are ca scop
  usurarea trimiterii/primirii unui cantitati fixe de date. Aceste
  functii au declaratiile urmatoare:
  ,----
  | void send(const void* data, ssize_t size);
  | void recv(void* data, ssize_t size);
  `----

  Functiile iau ca parametru un buffer de date si marimea pe care dorim
  sa o trimitem, si vor folosi apeluri repetate ale functiilor
  `send=/=recv', si un cap de citire/scriere pentru a se asigura ca pe
  retea ajunge tot bufferul.

  Exemplu send:
  ,----
  | void tcp_socket::send(const void* data, ssize_t size) {
  | 
  |     ssize_t sent = 0;
  |     u8* write_head = (u8*)data;
  |     while (sent != size) {
  | 
  |         ssize_t ret = CCALL(::send(_fd, write_head, size - sent, 0),
  |                             "Failed to send to socket");
  |         if (ret == 0) {
  |             throw 0;
  |         }
  | 
  |         sent += ret;
  |         write_head += ret;
  |     }
  `----

  Functia pentru read arata foarte asemanator, singura diferenta fiind
  faptul ca se apeleaza recv in loc de send.


4.1.2 Trimiterea datelor pe retea (2)
-------------------------------------

  Pentru a usura si mai mult transmiterea de date pe retea am ales sa
  implementez un API de serializare/deserializare pentru anumite tipuri
  de date din C++. Ca si tipuri de date am ales tipurile "triviale" (de
  ex. int, double, structuri simple) si vectori STL de tipuri triviale.
  Acest API se afla in fisierul <./liv_serialize.hpp>.

  Exemplu deserializare vector de tipuri triviale:
  ,----
  | template <typename T>
  | requires(!is_trivial_v<T>) void deserialize(tcp_socket& sock, T& v) {
  |     size_t vec_size;
  |     deserialize(sock, vec_size);
  |     v.resize(vec_size);
  |     sock.recv(v.data(), sizeof(decltype(*begin(v))) * vec_size);
  | }
  `----


4.1.3 Structura pachetelor UDP
------------------------------

  Un pachet UDP arata in felul urmator:
  ,----
  | class liv_udp_packet {
  | public:
  |     /* snipped */
  | private:
  |     string topic;
  |     vector<u8> data;
  |     u8 data_type;
  |     u16 sender_port;
  |     u32 sender_addr;
  | };
  `----

  Deoarece acest pachet trebuie sa calatoreasca si pe socketi UDP si
  TCP, am ales sa implementez in interiorul clasei doar transmiterea
  peste socketi TCP. Pentru primirea pachetelor de pe socketul UDP se
  ocupa clasa `liv_udp_sink'. Pentru transferurile peste TCP exista un
  api de trimitere/primire ale pachetului:
  ,----
  |     void serialize_to(tcp_socket& sock);
  |     void deserialize_from(tcp_socket& sock);
  `----

  Primirea pachetelor UDP de pe socketul UDP se face in felul urmator:
  ,----
  |     u8 tmp_buf[2048];
  | 
  |     sockaddr_in sender;
  |     socklen_t sender_len = sizeof(sender);
  | 
  |     ::memset(&sender, 0, sizeof(sender));
  | 
  |     ssize_t received = CCALL(::recvfrom(_fd, tmp_buf, sizeof(tmp_buf), 0,
  |                                         (sockaddr*)&sender, &sender_len),
  |                              "Failed to read from udp sink socket");
  |     char tmp_topic[51];
  |     ::sprintf(tmp_topic, "%.50s", (char*)tmp_buf);
  |     gte(received, 51L, "Did not receive enough udp data");
  |     return liv_udp_packet(tmp_topic, tmp_buf[50], &tmp_buf[51], received - 51,
  |                           sender);
  `----
  - Se se citesc de pe socket datele, si sunt folosite pentru
    construirea unui pachet.


4.1.4 Structura pachetelor TCP
------------------------------

  Datele tinute in pachetele TCP sunt structurate in felul urmator:
  ,----
  | class liv_tcp_packet {
  |     /* snipped */
  | private:
  |     packet_type type;
  |     payload_variant payload;
  | };
  `----

  Acesta poate fi trimis sau primit de pe retea folosind un
  `tcp_socket':
  ,----
  |     void serialize_to(tcp_socket& sock);
  |     void deserialize_from(tcp_socket& sock);
  `----

  Datorita faptului ca in protocolul definit de mine pot sa existe mai
  multe tipuri de pachete cu continuturi complet diferite, am ales sa
  folosesc clasa `variant' disponibila in C++, care poate sa contina mai
  multe tipuri de date si garanteaza ca odata ce un tip de date este
  plasat in ea, doar o referinta catre acel tip poate fi obtinuta,
  generandu-se erori in cazul in care se cere altceva.

  Tipul `payload_variant':
  ,----
  | using payload_variant =
  |     variant<message_payload, management_payload, id_payload>;
  `----

  Pachetele TCP pot sa contina trei tipuri de mesaje:
  - care contin un pachet UDP `message_payload'
  - care contin pachete de management (subscribe/unsubscribe)
    `management_payload'
  - care contin ID-ul subscriberului (generat atunci cand subscriberul
    se conecteaza la server) `id_payload'

  Aceste payload-uri contin la urma lor:


* 4.1.4.1 management _ payload

  ,----
  | struct management_payload {
  |     int sf; /* store-and-forward flag */
  |     string verb; /* comanda trimisa */
  |     string topic; /* topicul pe care se doreste efectuarea comenzii */
  | };
  `----


* 4.1.4.2 message _ payload

  ,----
  | struct message_payload {
  |     liv_udp_packet packet; /* Pachet UDP */
  | };
  `----


* 4.1.4.3 id _ payload

  ,----
  | struct id_payload {
  |     string user_id; /* id-ul subscriber-ului */
  | };
  `----


4.1.5 Memory management
-----------------------

  Datorita faptului ca mesajele netrimise trebuie stocate eficient (sa
  nu exista cate o copie a intregului mesaj pentru fiecare sf-queue), si
  faptului ca subscriberul trebuie sa poata fi accesat din mai multe
  structuri de date, am ales sa folosesc smart pointers din C++, mai
  exact `shared_ptr', care ofera un pointer care isi numara referintele
  active, iar cand acest numar ajunge zero apeleaza destructorul clasei
  pe care o tine si elibereaza memoria in mod automat.


4.2 Server
~~~~~~~~~~

  Atunci cand exista o conexiune noua se pe server se efectueaza
  urmatoarele lucruri:
  - Se asteapta pana de pe conexiunea noua se poate citi un
    `liv_tcp_packet' care contine id-ul subscriberului.
  - Se verifica daca exista un subscriber deja conectat cu acelasi id.
    In caz ca exista conexiunea este inchisa si se afiseaza pe consola
    serverului un mesaj corespunzator.
  - Se updateaza/adauga subscriberul in `unordered_map'

  Pentru a tine usor evidenta subscriberilor care s-au mai conectat pana
  acum am folosit un `unordered_map' care mapeaza id-uri la subscriberi.
  Astfel dupa ce s-a primit id-ul de la o conexiune noua se poate
  verifica daca acest utilizator a mai fost conectat.

  Pentru a tine evidenta topicurilor la care este abonat un subscriber,
  am folosit din nou un `unordered_map', care mapeaza numele topicului
  la un `unordered_map' care mapeaza id-ul subscriberului la o pereche
  cu un `shared_ptr' catre subscriber si flagul store and forward.

  ,----
  | /* snipped */
  | struct client_entry {
  |     shared_ptr<liv_client> client_ptr;
  |     bool sf;
  | };
  | /* snipped */
  | unordered_map<string, unordered_map<string, client_entry>> buckets;
  `----

  Astfel toate cautarile/adaugarile/scoaterile se efectueaza cu o
  complexitate in timp constanta.

  Pentru a indeplini functia de broker de mesaje serverul foloseste
  urmatorul API aflat in <./liv_client_manager.hpp>

  ,----
  | void distribute(shared_ptr<liv_tcp_packet> packet);
  | void subscribe(shared_ptr<liv_client> client, string topic, bool sf);
  | void unsubscribe(shared_ptr<liv_client> client, string topic);
  `----
