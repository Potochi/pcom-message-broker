#pragma once

#include "common.hpp"
#include "liv_socket.hpp"
#include "liv_tcp_packet.hpp"
#include <arpa/inet.h>
#include <iostream>
#include <string>
#include <sys/socket.h>
using namespace std;
using namespace liv;

enum input_type { USER_INPUT, MESSAGE_INPUT, ERROR };
enum user_action { SUBSCRIBE, UNSUBSCRIBE, INVALID };
class liv_subscriber {
public:
    liv_subscriber(const string& id, const string& ip, const string& port);
    void run();

private:
    input_type await_input();
    void handle_message();
    void handle_user_input();
    tcp_socket tsock;
    string id;
    fd_set readfds;
    int maxfd;
    int running;
};
