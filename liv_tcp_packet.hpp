#pragma once

#include "common.hpp"
#include "liv_socket.hpp"
#include "liv_udp_packet.hpp"
#include <variant>

using namespace std;

namespace liv {

enum packet_type { MANAGEMENT, MESSAGE, ID };

struct management_payload {
    int sf;
    string verb;
    string topic;
};

struct message_payload {
    message_payload() = default;
    message_payload(liv_udp_packet&& packet) : packet(packet) {}
    liv_udp_packet packet;
};

struct id_payload {
    string user_id;
};

using payload_variant =
    variant<message_payload, management_payload, id_payload>;

class liv_tcp_packet {
public:
    liv_tcp_packet() = default;
    liv_tcp_packet(management_payload&& management);
    liv_tcp_packet(message_payload&& message);
    liv_tcp_packet(id_payload&& id);
    liv_tcp_packet(liv_udp_packet&& packet);

    void serialize_to(tcp_socket& sock);
    void deserialize_from(tcp_socket& sock);

    packet_type get_type() const;
    const payload_variant& get_payload() const;

private:
    packet_type type;
    payload_variant payload;
};

} // namespace liv
