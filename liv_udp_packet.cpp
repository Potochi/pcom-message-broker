#include "liv_udp_packet.hpp"
#include "liv_serialize.hpp"

liv_udp_packet::liv_udp_packet() : topic(), data() {}

liv_udp_packet::liv_udp_packet(const char* topic, u8 data_type, u8* raw_data,
                               size_t data_size)
    : topic(topic), data_type(data_type), sender_port(0), sender_addr(0) {

    data.resize(data_size);
    copy(raw_data, raw_data + data_size, data.begin());
}

liv_udp_packet::liv_udp_packet(const char* topic, u8 data_type, u8* raw_data,
                               size_t data_size, const sockaddr_in& sender)
    : topic(topic), data_type(data_type), sender_port(0), sender_addr(0) {

    data.resize(data_size);
    copy(raw_data, raw_data + data_size, data.begin());
    sender_addr = sender.sin_addr.s_addr;
    sender_port = sender.sin_port;
}

const string& liv_udp_packet::get_topic() const {
    return topic;
}

const vector<u8>& liv_udp_packet::get_data() const {
    return data;
}

u8 liv_udp_packet::get_data_type() const {
    return data_type;
}

u16 liv_udp_packet::get_port() const {
    return sender_port;
}

u32 liv_udp_packet::get_addr() const {
    return sender_addr;
}

void liv_udp_packet::serialize_to(tcp_socket& sock) {
    serialize(sock, topic);
    serialize(sock, data_type);
    serialize(sock, data);
    serialize(sock, sender_port);
    serialize(sock, sender_addr);
}

void liv_udp_packet::deserialize_from(tcp_socket& sock) {
    deserialize(sock, topic);
    deserialize(sock, data_type);
    deserialize(sock, data);
    deserialize(sock, sender_port);
    deserialize(sock, sender_addr);
}
