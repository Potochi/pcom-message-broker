#pragma once

#include "liv_types.hpp"
#include <iostream>

#include <cstring>
#include <errno.h>
#include <string>

using namespace std;

namespace liv {

void _internal_assert_fail(const string& msg);

template <typename T>
T CCALL(T v, const string& msg = "") {
    if (v < 0) {
        cerr << "C function call failed with: " << strerror(errno) << ", "
             << msg << endl;
        throw errno;
    }
    return v;
}

template <typename T>
void eq(const T& lhs, const T& rhs, const string& msg = "") {

    if (!(lhs == rhs)) {
        _internal_assert_fail(msg);
    }
}
template <typename T>
void neq(const T& lhs, const T& rhs, const string& msg = "") {

    if (!(lhs != rhs)) {
        _internal_assert_fail(msg);
    }
}
template <typename T>
void lte(const T& lhs, const T& rhs, const string& msg = "") {

    if (!(lhs <= rhs)) {
        _internal_assert_fail(msg);
    }
}
template <typename T, typename... Args>
void lt(const T& lhs, const T& rhs, const string& msg = "") {

    if (!(lhs < rhs)) {
        _internal_assert_fail(msg);
    }
}

template <typename T, typename... Args>
void gte(const T& lhs, const T& rhs, const string& msg = "") {

    if (!(lhs >= rhs)) {
        _internal_assert_fail(msg);
    }
}

template <typename T, typename... Args>
void gt(const T& lhs, const T& rhs, const string& msg = "") {

    if (!(lhs > rhs)) {
        _internal_assert_fail(msg);
    }
}
} // namespace liv
