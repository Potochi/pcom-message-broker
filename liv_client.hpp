#pragma once

#include "common.hpp"
#include "liv_socket.hpp"
#include "liv_tcp_packet.hpp"
#include <deque>
#include <optional>
#include <string>

using namespace liv;
using namespace std;

class liv_client {
public:
    liv_client(int fd);
    liv_client(tcp_socket&& sock);
    void disconnect();
    void set_id(string new_id);
    void send_packet(shared_ptr<liv_tcp_packet> packet, bool sf);
    void send_queue();
    shared_ptr<liv_tcp_packet> recv_packet();

    optional<tcp_socket> _sock;
    optional<string> id;
    deque<shared_ptr<liv_tcp_packet>> sf_queue;
};
