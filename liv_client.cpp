#include "liv_client.hpp"

#include <iostream>

using namespace std;
using namespace liv;

liv_client::liv_client(int fd) : _sock(fd) {}

liv_client::liv_client(tcp_socket&& sock) : _sock(sock) {}

void liv_client::disconnect() {
    if (_sock.has_value()) {
        cout << "Client " + id.value_or("UNK") << " disconnected." << std::endl;
        this->_sock->close();
        this->_sock = nullopt;
    }
}

void liv_client::set_id(string new_id) {
    eq(id.has_value(), false, "Trying to reallocate an ID, logic error");
    this->id = new_id;
}

void liv_client::send_packet(shared_ptr<liv_tcp_packet> packet, bool sf) {
    if (!_sock.has_value()) {
        if (sf) {
            sf_queue.push_back(packet);
        }
        return;
    }

    packet->serialize_to(_sock.value());
}

void liv_client::send_queue() {
    while (!sf_queue.empty()) {
        auto queued_packet = sf_queue.front();
        queued_packet->serialize_to(_sock.value());
        sf_queue.pop_front();
    }
}

shared_ptr<liv_tcp_packet> liv_client::recv_packet() {

    eq(_sock.has_value(), true,
       "Trying to read from disconnected client, logic error");

    auto packet = make_shared<liv_tcp_packet>();
    packet->deserialize_from(_sock.value());

    return packet;
}
