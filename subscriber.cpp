#include "common.hpp"
#include "liv_socket.hpp"
#include "liv_subscriber.hpp"
#include "liv_tcp_packet.hpp"
#include "liv_udp_packet.hpp"
#include <iostream>
#include <string>
using namespace std;
using namespace liv;

void usage() {
    cerr << "./subscriber <id> <ip> <port>\n";
}

int main(int argc, char** argv) {

    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);

    if (argc != 4) {
        usage();
        return -1;
    }

    try {
        liv_subscriber subscriber{argv[1], argv[2], argv[3]};
        subscriber.run();
    } catch (const invalid_argument& e) {
        cerr << "Invalid port" << endl;
    } catch (const out_of_range& e) {
        cerr << "Invalid port" << endl;
    }
}
