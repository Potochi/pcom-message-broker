#include "liv_client_manager.hpp"

#include <arpa/inet.h>
#include <iostream>
#include <netinet/tcp.h>
#include <stdio.h>
#include <unistd.h>

liv_client_manager::liv_client_manager(const string& port) : _max_fd(-1) {

    int cport = static_cast<int>(stoi(port));

    if (cport > numeric_limits<u16>::max()) {
        throw invalid_argument("Invalid port");
    }

    _udp_sink.emplace(cport);

    running = 1;

    memset(&_user_fds, 0, sizeof(_user_fds));

    _listen_fd = CCALL(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP),
                       "Failed to open tcp listener");

    _udp_fd = _udp_sink->_fd;
    set_socket(_udp_fd);
    set_socket(_listen_fd);
    set_socket(STDIN_FILENO);

    const int yes = 1;
    CCALL(setsockopt(_listen_fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)),
          "Failed to set SO_REUSEADDR on tcp socket");

    CCALL(::setsockopt(_listen_fd, SOL_TCP, TCP_NODELAY, &yes, sizeof(yes)),
          "Failed to set TCP_NODELAY");

    sockaddr_in inaddr;
    memset(&inaddr, 0, sizeof(inaddr));

    inaddr.sin_addr.s_addr = INADDR_ANY;
    inaddr.sin_family = AF_INET;
    inaddr.sin_port = htons(cport);

    CCALL(::bind(_listen_fd, (sockaddr*)&inaddr, sizeof(inaddr)),
          "Failed to bind tcp socket");
    CCALL(::listen(_listen_fd, 10), "Failed to start listening");
}

void liv_client_manager::accept_connection() {

    sockaddr_in sock;
    socklen_t sock_len = sizeof(sock);

    int newsock;

    try {
        newsock = CCALL(::accept(_listen_fd, (sockaddr*)&sock, &sock_len),
                        "Failed to accept socket connection!");
        set_socket(newsock);

        const int yes = 1;

        CCALL(::setsockopt(newsock, SOL_TCP, TCP_NODELAY, &yes, sizeof(yes)),
              "Failed to set TCP_NODELAY");

        tcp_socket new_socket{newsock};

        liv_tcp_packet packet;
        packet.deserialize_from(new_socket);

        eq(packet.get_type(), ID, "Client did not send ID packet first");

        const auto& id = get<id_payload>(packet.get_payload());

        if (clients.contains(id.user_id)) {
            auto& client = clients.at(id.user_id);
            if (client->_sock.has_value()) {
                cout << "Client " << id.user_id << " already connected."
                     << endl;
                throw new_socket;
            }

            client->_sock = move(new_socket);
            try {
                client->send_queue();
            } catch (int x) {
                throw new_socket;
            }

        } else {
            auto new_client = make_shared<liv_client>(move(new_socket));

            new_client->set_id(id.user_id);

            clients.insert({id.user_id, new_client});
        }

        cout << "New client " << id.user_id << " connected from "
             << ::inet_ntoa(sock.sin_addr) << ":" << ::ntohs(sock.sin_port)
             << "." << endl;
    } catch (int x) {
        unset_socket(newsock);

    } catch (tcp_socket& socket) {
        unset_socket(socket._fd);
        socket.close();
    }
}
void liv_client_manager::read_packets(fd_set read_ready) {

    if (FD_ISSET(_listen_fd, &read_ready)) {
        FD_CLR(_listen_fd, &read_ready);
        accept_connection();
    }

    if (FD_ISSET(_udp_fd, &read_ready)) {
        FD_CLR(_udp_fd, &read_ready);
        read_udp();
    }

    for (auto& cl : clients) {
        if (cl.second->_sock.has_value() &&
            FD_ISSET(cl.second->_sock.value()._fd, &read_ready)) {
            try {
                auto packet = cl.second->recv_packet();

                switch (packet->get_type()) {
                case ID: {
                    auto& id = get<id_payload>(packet->get_payload());

                    cl.second->set_id(id.user_id);
                } break;
                case MANAGEMENT: {
                    auto& man = get<management_payload>(packet->get_payload());

                    if (man.verb == "subscribe") {
                        subscribe(cl.second, man.topic, man.sf);
                    } else if (man.verb == "unsubscribe") {
                        unsubscribe(cl.second, man.topic);
                    }

                } break;
                case MESSAGE: {
                    cerr << "Received unexpected packet from client";
                } break;
                }
            } catch (int x) {
                unset_socket(cl.second->_sock->_fd);
                cl.second->disconnect();
            }
        }
    }

    if (FD_ISSET(STDIN_FILENO, &read_ready)) {
        string command;
        cin >> command;

        if (command == "exit") {
            running = 0;
        }
    }
}

void liv_client_manager::read_udp() {
    auto packet = make_shared<liv_tcp_packet>(_udp_sink->receive_packet());

    distribute(packet);
}

void liv_client_manager::run() {
    while (running) {
        fd_set read_fds = _user_fds;

        CCALL(select(_max_fd + 1, &read_fds, NULL, NULL, NULL),
              "Failed to select sockets");
        read_packets(read_fds);
    }

    for (const auto& kv : clients) {
        kv.second->disconnect();
    }
}

void liv_client_manager::distribute(shared_ptr<liv_tcp_packet> packet) {
    eq(packet->get_type(), MESSAGE,
       "Trying to distribute non MESSAGE packet, logic error");

    auto& message = get<message_payload>(packet->get_payload());

    if (!buckets.contains(message.packet.get_topic())) {
        return;
    }

    auto& bucket = buckets.at(message.packet.get_topic());

    for (auto& entry : bucket) {
        try {
            entry.second.client_ptr->send_packet(packet, entry.second.sf);
        } catch (int x) {
            unset_socket(entry.second.client_ptr->_sock->_fd);
            entry.second.client_ptr->disconnect();
        }
    }
}

void liv_client_manager::subscribe(shared_ptr<liv_client> client, string topic,
                                   bool sf) {
    if (!buckets.contains(topic)) {
        buckets.insert({topic, {}});
    }

    auto& bucket = buckets.at(topic);

    if (!client->id.has_value()) {
        return;
    }

    if (bucket.contains(client->id.value())) {
        bucket.at(client->id.value()).sf = sf;
    } else {
        bucket.insert({client->id.value(), client_entry{client, sf}});
    }
}

void liv_client_manager::unsubscribe(shared_ptr<liv_client> client,
                                     string topic) {

    if (!buckets.contains(topic)) {
        return;
    }

    auto& bucket = buckets.at(topic);

    const auto& to_unsub = bucket.find(client->id.value());

    if (to_unsub == bucket.end()) {
        return;
    }

    bucket.erase(to_unsub);
}

void liv_client_manager::set_socket(int fd) {
    if (fd > _max_fd) {
        _max_fd = fd;
    }

    FD_SET(fd, &_user_fds);
}

void liv_client_manager::unset_socket(int fd) {
    FD_CLR(fd, &_user_fds);
}
